app.service('Constants', function ($rootScope) {

  //timezone
  var currentTime = new Date();
  var zone = currentTime.getTimezoneOffset();
  this.timeZone = (zone / 60) * -1;

  this.platformId = 5;
  this.keyUserId = "keyUserId";
  this.monthsString = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

  this.userId = '';
  this.notificationId = '';
  this.deviceId = undefined
  this.server = 'http://hellotipsters.com/demo1_hellotipsters.com/php'

});

app.service('DataServices', function ($rootScope, $http, Constants) {

  this.login = function () {
    var params = {};
    params.platform_id = Constants.platformId;
    params.device_id = Constants.deviceId;
    params.notification_id = Constants.notificationId;
    console.log("params ", params);

    return $http.post(Constants.server + "/users/mr_fixit/users_login.php", params)
      .success(function (data) {
        console.log('login success');
      }).error(function (data) {
        console.log('login fail');
        console.log(data);
      })
  };

  this.getPost = function () {
    console.log(Constants.userId);
    return $http.get(Constants.server + "/posts/posts_get_picks.php?platform_id=" + Constants.platformId + "&user_id=" + Constants.userId)
      .success(function (data) {
        console.log('get user posts success');
      }).error(function (data) {
        console.log('get user posts error');
        console.log(data);
      });
  }

  this.getFreebets = function () {
    var params = {};
    params.platform = Constants.platformId;
    return $http.get(Constants.server + "/free_bets/freebets_get.php?platform_id=" + params.platform)
      .success(function (data) {
        console.log('get freebets success');
      }).error(function (data) {
        console.log('get freebets fail');
        console.log(data);
      })
  };

  //livescore
  this.getLiveScores = function () {
    var params = {};
    params.time_zone = Constants.timeZone;
    params.id = Constants.platformId
    params.user_id = Constants.userId;
    // console.log(params);
    return $http.post(Constants.server + "/scores/mr_fixit/scores_live_app.php", params)
      .success(function (data) {
        //console.log('get live scores success');
      }).error(function (data) {
        //console.log('get live scores fail');
        //console.log(data);
      })
  };

  this.getAllScores = function (date) {
    var params = {};
    params.time_zone = Constants.timeZone;
    params.date = date;
    params.id = Constants.platformId
    params.user_id = Constants.userId;
    console.log(params);

    return $http.post(Constants.server + "/scores/mr_fixit/scores_result_and_future_app.php", params)
      .success(function (data) {
        console.log('get live scores success');
      }).error(function (data) {
        console.log('get live scores fail');
        console.log(data);
      })
  };

  this.getLeagues = function () {
    var params = {};
    params.id = Constants.platformId;

    return $http.post(Constants.server + "/scores/league_get.php", params)
      .success(function (data) {
        console.log('get Filtred scores success');
      }).error(function (data) {
        console.log('get Filtred scores fail');
        console.log(data);
      })
  };

  this.getLeagueScores = function (league, date) {
    var params = {};
    params.time_zone = Constants.timeZone;
    params.league = league;
    params.date = date;
    params.user_id = Constants.userId;

    console.log(params);

    return $http.post(Constants.server + "/scores/mr_fixit/scores_league_get.php", params)
      .success(function (data) {
        console.log('comment success');
      }).error(function (data) {
        console.log('comment fail');
        console.log(data);
      })
  };

  this.follow = function (event) {
    var params = {};
    params.user_id = Constants.userId;
    params.event_id = event;

    console.log(params);

    return $http.post(Constants.server + "/scores/scores_add_follow.php", params)
      .success(function (data) {
        console.log('follow success');
      }).error(function (data) {
        console.log('follow fail');
        console.log(data);
      })
  };

  this.removeFollow = function (follow) {
    var params = {};
    params.follow_id = follow;

    console.log(params);

    return $http.post(Constants.server + "/scores/scores_remove_follow.php", params)
      .success(function (data) {
        console.log('removeFollow success');
      }).error(function (data) {
        console.log('removeFollow fail');
        console.log(data);
      })
  };

  this.notification = function (event) {
    var params = {};
    params.user_id = Constants.userId;
    params.event_id = event;

    console.log(params);

    return $http.post(Constants.server + "/scores/scores_add_notification.php", params)
      .success(function (data) {
        console.log('notification success');
      }).error(function (data) {
        console.log('notification fail');
        console.log(data);
      })
  };

  this.removeNotification = function (notification) {
    var params = {};
    params.notification_id = notification;

    console.log(params);

    return $http.post(Constants.server + "/scores/scores_remove_notification.php", params)
      .success(function (data) {
        console.log('removeNotification success');
      }).error(function (data) {
        console.log('removeNotification fail');
        console.log(data);
      })
  };

  this.addLike = function (postId, type) {
    console.log(postId + " " + Constants.userId + " " + type)
    return $http.get(Constants.server + "/posts/posts_like_add.php?post_id=" + postId + "&user_id=" + Constants.userId + "&type=" + type)
      .success(function (data) {
        console.log('like success');
      }).error(function (data) {
        console.log('comment fail');
        console.log(data);
      })
  };

  this.getUserSettings = function () {
    var params = {};
    params.platform_id = Constants.platformId;
    params.user_id = Constants.userId;

    return $http.post(Constants.server + "/users/user_setting.php", params)
      .success(function (data) {
        console.log('settings succes');
      }).error(function (data) {
        console.log('settings fail');
        console.log(data);
      })
  };

  this.updateSettings = function (models) {
    var params = {};
    params.platform_id = Constants.platformId;
    params.user_id = Constants.userId;
    params.data = models;
    console.log(params);

    return $http.post(Constants.server + "/users/update_user_setting.php", params)
      .success(function (data) {
        console.log('settings succes');
      }).error(function (data) {
        console.log('settings fail');
        console.log(data);
      })
  };

});