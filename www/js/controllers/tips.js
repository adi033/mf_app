app.controller('tips', function ($scope, $state, DataServices, $ionicLoading, $rootScope, $interval) {
  $scope.tips = {
    arr: [],
  }

  $scope.$on("$ionicView.loaded", function (event) {
    getPost(true);
  });

  $scope.$on("$ionicView.enter", function (event) {
    $scope.postRefresh = $interval(() => {
      getPost(false);
    }, 60000, 0);
  });

  $scope.$on("$ionicView.leave", function (event) {
    $interval.cancel($scope.postRefresh);
  });

  function getPost(freshLoad) {
    if (freshLoad) {
      $ionicLoading.show();

      if (!$rootScope.onlineState) {
        $ionicLoading.hide();
        return $rootScope.ShowAlertPopup('No connection', 'App can behave strange, please reconnect');
      }
    }

    DataServices.getPost()
      .success(function (data) {
        console.log("data ", data);
        $scope.tips.arr = data;
      })
      .error(function () {
        //alert("Something went wrong. Please refresh and try again.");
      })
      .then(function () {
        $ionicLoading.hide();
      })
  }

  $scope.onLikes = function (postId, index) {

    if (!$rootScope.onlineState) {
      $ionicLoading.hide();
      return $rootScope.ShowAlertPopup('No connection', 'App can behave strange, please reconnect');
    }

    if ($rootScope.loginCheck()) {
      //console.log("$scope.params.posts[index] ", $scope.params.posts[index]);
      if ($scope.tips.arr[index].post.activeLike == 0) {
        $scope.tips.arr[index].post.activeLike = 1;
        $scope.tips.arr[index].post.likes++;
      }
      else {
        $scope.tips.arr[index].post.activeLike = 0;
        $scope.tips.arr[index].post.likes--;
      }

      DataServices.addLike(postId, 3)
        .success(function (data) {
          //$scope.getPost($scope.USERID);
        })
        .error(function () {
          console.log("Something went wrong. Please refresh and try again.");
        })
    }
  }

  $scope.onBtnArrow = (post) => {
    $state.go('tabs.activeTip', { post: post });
  }

  $scope.onBtnShare = () => {
    console.log('share');
    window.plugins.socialsharing.share('try my app');
  }

});

