app.controller('settings', function ($scope, $state, $rootScope, DataServices, $ionicLoading) {

  $scope.models = {
    notification: undefined,
    changed: ''
  }

  $scope.$on("$ionicView.loaded", function (event) {
    getSettings();
  });

  $scope.onPushSwitch = function () {
    console.log($scope.models.notification);
    $scope.models.changed = "notification";
    updateSettings();
  }

  updateSettings = () => {

    if (!$rootScope.onlineState) {
      $ionicLoading.hide();
      return alert("No connection");
    }

    DataServices.updateSettings($scope.models)
      .success(function (data) {
        console.log(data);
      })
      .error(function () {
        console.log("Something went wrong. Please refresh and try again.");
      })
  }

  getSettings = () => {
    $ionicLoading.show();
    if (!$rootScope.onlineState) {
      $ionicLoading.hide();
      return;
    }

    DataServices.getUserSettings()
      .success(function (data) {
        console.log(data);
        //$scope.models.notification = data[0].notification == "true" ? true : false;
      })
      .error(function () {
        console.log("Something went wrong. Please refresh and try again.");
      })
      .then(() => {
        $ionicLoading.hide();
      })
  }
});

