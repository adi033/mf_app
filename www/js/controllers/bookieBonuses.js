app.controller('bookieBonuses', function ($scope, DataServices, $rootScope, $ionicLoading, $interval) {

  $scope.freebets = {
    arr: [],
  }

  $scope.$on("$ionicView.loaded", function (event) {
    getFreeBets(true);
  });

  $scope.$on("$ionicView.enter", function (event) {
    $scope.freeBetsRefresh = $interval(() => {
      getFreeBets(false);
    }, 60000, 0);
  });

  $scope.$on("$ionicView.leave", function (event) {
    $interval.cancel($scope.freeBetsRefresh);
  });

  function getFreeBets(freshLoad) {
    if (freshLoad) {
      $ionicLoading.show()
      if (!$rootScope.onlineState) {
        $ionicLoading.hide();
        return;
      }
    }

    DataServices.getFreebets()
      .success(function (data) {
        $scope.freebets.arr = data;
        console.log($scope.freebets);
      })
      .error(function () {
        console.log("Something went wrong. Please refresh and try again.");
      })
      .then(function () {
        $ionicLoading.hide();
      })
  }

  $scope.goToPage = (link) => {
    // window.location.href = link;
    window.open(link,'_blank');
  }

});

