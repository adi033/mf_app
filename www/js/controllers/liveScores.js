app.controller('liveScores', function ($scope, $ionicSideMenuDelegate, DataServices, $ionicLoading, $filter, $state, $rootScope, $ionicScrollDelegate, $interval) {
  $scope.openMenu = function () {
    $ionicSideMenuDelegate.toggleLeft();
  };

  $scope.params = {};
  $scope.params.scores = [];
  $scope.toggle = "live";

  $scope.calendar = {}
  $scope.calendar.toggle = "1";
  $scope.calendar.days = [];
  $scope.calendar.nameOfDays = [];
  //$scope.selectedDay;

  //diltred inputs
  $scope.params.dateFrom;
  $scope.params.dateTo;

  $scope.dateToSend;

  $scope.params.showColorBell = [];
  $scope.params.showColorStar = [];

  $scope.params.leagues = [];
  $scope.params.pickedLeague = "English Premier League";

  $scope.$watch('toggle', function () {
    $rootScope.liveScore.toggle = $scope.toggle;
    ////console.log("$rootScope.liveScore.toggle ", $rootScope.liveScore.toggle);
    $ionicScrollDelegate.$getByHandle('scoreScroll').scrollTop();
  }, true);

  $scope.$on("$ionicView.enter", function (event) {
    $scope.toggle = "live";
    $scope.calendar.toggle = "1";
    $ionicLoading.show();
    $scope.getLiveScores();
    liveScoreRefresh()
    //Run to highlit first date;
    $scope.dateToSend = $scope.dateToPhp();
  });

  $scope.onBtnAll = function () {
    $ionicLoading.show();
    $scope.toggle = 'all';

    if ($scope.dateToSend == undefined)
      $scope.dateToSend = $scope.dateToPhp();
    //$scope.dateToSend = $scope.dateToPhp();
    console.log($scope.dateToSend);

    $scope.getAllScores($scope.dateToSend);
  }

  $scope.onBtnLive = function () {
    $ionicLoading.show();
    $scope.toggle = 'live';
    liveScoreRefresh()
    $scope.dateToSend = $scope.dateToPhp();
    $scope.getLiveScores();
  }

  $scope.onBtnFiltred = function () {
    $scope.toggle = 'filtred';
    $scope.params.scores.splice(0, $scope.params.scores.length);

    DataServices.getLeagues()
      .success(function (data) {
        $scope.params.leagues = data;
        //console.log($scope.params.leagues);
      })
      .error(function () {
        //console.log("Something went wrong. Please refresh and try again.");
      })
  }

  $scope.onBtnFilter = function () {
    $ionicLoading.show();
    if ($scope.dateToSend == undefined)
      $scope.dateToSend = $scope.dateToPhp();

    $scope.getLeagueScore();
  }

  //for highliting picked day 
  $scope.isSelected = function (day) {
    return $scope.selectedDay === day;
  }

  //select day for and class and getting live sccore dat day
  $scope.onClickedDay = function (day) {
    $ionicScrollDelegate.$getByHandle('scoreScroll').scrollTop();
    //set active button to all
    //console.log($scope.toggle);
    if ($scope.toggle == 'filtred') {
      $scope.dateToSend = $scope.dateToPhp(day);
      $scope.selectedDay = day;
      $scope.params.scores.splice(0, $scope.params.scores.length);
      $scope.getLeagueScore();
    }
    else {
      $ionicLoading.show();
      $scope.toggle = 'all';
      //console.log($scope.toggle);
      $scope.selectedDay = day;
      $scope.dateToSend = $scope.dateToPhp(day);
      //console.log("date " + $scope.dateToSend);
      $scope.getAllScores($scope.dateToSend)
    }
  }

  $scope.onIconChat = function () {
    $state.go("chat");
  }

  $scope.onImgStar = function (event, follow) {
    console.log(event, follow);
    if (follow != -1) {
      //console.log('remove follow');
      DataServices.removeFollow(follow)
        .success(function (data) {
          //console.log(JSON.stringify(data));
        })
        .error(function () {
          //console.log("Something went wrong. Please refresh and try again.");
        })
    }
    else {
      //console.log('add follow');
      DataServices.follow(event)
        .success(function (data) {
          //console.log(data);
          $ionicScrollDelegate.$getByHandle('scoreScroll').scrollTop();
        })
        .error(function () {
          //console.log("Something went wrong. Please refresh and try again.");
        })
    }

    $scope.switchCallLiveScore();
  }

  $scope.onImgBell = function (event, notification, index) {
    console.log("index ", index);
    console.log("event ", event);
    console.log("notification ", notification);
    var notificationId = notification;

    if (notification != -1) {
      //console.log('remove notification');

      $scope.params.scores[index].event.notification = -1;
      ////console.log("$scope.params.scores[index].event.notification ", $scope.params.scores[index].event.notification);

      DataServices.removeNotification(notificationId)
        .success(function (data) {
          //console.log(data);
        })
        .error(function () {
          //console.log("Something went wrong. Please refresh and try again.");
        })

    }
    else {
      //console.log('add notification');
      $scope.params.scores[index].event.notification = 1;
      DataServices.notification(event)
        .success(function (data) {
          $scope.params.scores[index].event.notification = data[0].id;
          //console.log("$scope.params.scores[index].event.notification ", $scope.params.scores[index].event.notification);
        })
        .error(function () {
          //console.log("Something went wrong. Please refresh and try again.");
        })
    }

    //$scope.switchCallLiveScore();
  }

  $scope.switchCallLiveScore = function () {
    $ionicLoading.show();
    switch ($scope.toggle) {
      case 'all':
        if ($scope.dateToSend == undefined)
          $scope.dateToSend = $scope.dateToPhp();

        $scope.getAllScores($scope.dateToSend);
        break;

      case 'live':
        $scope.dateToSend = $scope.dateToPhp();
        $scope.getLiveScores();
        break;

      case 'filtred':
        if ($scope.dateToSend == undefined)
          $scope.dateToSend = $scope.dateToPhp();

        $scope.getLeagueScore();
        break;
    }
  }

  $scope.getLeagueScore = function () {
    $ionicLoading.show();

    if (!$rootScope.onlineState) {
      $ionicLoading.hide();
      return;
    }

    DataServices.getLeagueScores($scope.params.pickedLeague, $scope.dateToSend)
      .success(function (data) {
        $scope.params.scores = data;
        console.log(data);
        $ionicLoading.hide();
      })
      .error(function () {
        //console.log("Something went wrong. Please refresh and try again.");
        $ionicLoading.hide();
      })
  }

  $scope.getAllScores = function (date) {
    if (!$rootScope.onlineState) {
      $ionicLoading.hide();
      return;//alert("No Connection, please reconnect");
    }

    console.log(date);

    DataServices.getAllScores(date)
      .success(function (data) {
        console.log(data);
        // //console.log(JSON.stringify(data[0].future));
        var future = data[0].future;
        var result = data[0].result;

        var newArray = result.concat(future);
        $scope.params.scores = newArray;
        // $scope.params.scores = data;
        $ionicLoading.hide();
      })
      .error(function () {
        //console.log("Something went wrong. Please refresh and try again.");
        $ionicLoading.hide();
      })
  }

  $scope.fillDaysArr = function () {
    var today = new Date();
    var ActualDate = today.getDate();
    for (var i = 0; i < 7; i++) {
      today.setDate(ActualDate);
      var tomorrow = new Date();
      tomorrow.setDate(today.getDate() + i);
      ////console.log(tomorrow.getDate());
      $scope.calendar.days.push(tomorrow.getDate());
    }
    ////console.log($scope.calendar.days);
  }();

  $scope.getLiveScores = function () {
    if (!$rootScope.onlineState) {
      $ionicLoading.hide();
      return;//alert("No Connection, please reconnect");
    }

    DataServices.getLiveScores()
      .success(function (data) {
        console.log("data ", data);
        $scope.params.scores = data;
        $ionicLoading.hide();
      })
      .error(function () {
        //console.log("Something went wrong. Please refresh and try again.");
        $ionicLoading.hide();
      })
  }

  $scope.fillNameofDaysArr = function () {
    var today = new Date();
    var ActualDate = today.getDate();
    for (var i = 0; i < 7; i++) {
      today.setDate(ActualDate);
      var tomorrow = new Date();
      tomorrow.setDate(today.getDate() + i);

      switch (tomorrow.getDay()) {
        case 0:
          den = 'S';
          break;
        case 1:
          den = 'M';
          break;
        case 2:
          den = 'T';
          break;
        case 3:
          den = 'W';
          break;
        case 4:
          den = 'T';
          break;
        case 5:
          den = 'F';
          break;
        case 6:
          den = 'S';
          break;
        default:
          alert('error');
      }
      $scope.calendar.nameOfDays.push(den);
    }
    ////console.log($scope.calendar.nameOfDays);
  }();

  //transfrom ng model date to db like
  $scope.dateToPhp = function (day) {
    if (day) {
      //console.log('day set');
    }
    else {
      //if day isnt set get today
      //console.log('date is not set');
      var today = new Date();
      var ActualDate = today.getDate();
      today.setDate(ActualDate);
      var tomorrow = new Date();
      tomorrow.setDate(today.getDate());
      ////console.log(tomorrow.getDate());
      day = tomorrow.getDate();
      $scope.selectedDay = day;
      $scope.isSelected();
    }

    var DatumToPhp = new Date();
    var date = new Date();
    var date2 = new Date();
    var HelpDate = new Date();

    date2.setDate(day);

    ////console.log(date.getTime())

    var timeDiff = Math.abs(date.getTime() - date2.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

    ////console.log(diffDays);

    //ked na druhy mesiac
    if (diffDays > 6) {
      var TestDate = date.getDate() - diffDays;
      ////console.log(TestDate);
      DatumToPhp.setDate(TestDate);
      DatumToPhp.setMonth(DatumToPhp.getMonth() + 1);
      ////console.log(DatumToPhp);
    }
    else {
      DatumToPhp.setDate(DatumToPhp.getDate() + diffDays);
    }

    var dd = DatumToPhp.getDate();
    var mm = DatumToPhp.getMonth() + 1; //January is 0!
    var yyyy = DatumToPhp.getFullYear();

    if (dd < 10) {
      dd = '0' + dd
    }

    if (mm < 10) {
      mm = '0' + mm
    }

    DatumToPhp = yyyy + '-' + mm + '-' + dd;

    ////console.log(DatumToPhp);
    return DatumToPhp;
  }

  liveScoreRefresh = () => {
    let refresh = $interval(() => {
      if ($scope.toggle == 'live')
        $scope.getLiveScores();
      else
        $interval.cancel(refresh);
    }, 10000, 0);
  }

});