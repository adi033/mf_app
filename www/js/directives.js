app.directive('iframeOnload', [function () {
  return {
    scope: {
      callBack: '&iframeOnload'
    },
    link: function (scope, element, attrs) {
      element.on('load', function () {
        return scope.callBack();
      })
    }
  }
}])

app.filter('oddsConventor', function () {
  return function (odds) {
    let usOdds = 0
    let decimalOdds = odds

    if (decimalOdds == 0) return '0';
    // console.log("decimalOdds ", decimalOdds);

    if (decimalOdds < 2) {
      usOdds = (-100) / (decimalOdds - 1)
      // console.log("usOdds ", usOdds);
    }
    else {
      usOdds = (decimalOdds - 1) * 100
      // console.log("usOdds ", usOdds);
    }
    return Math.round(usOdds)
  }
})

app.filter("myDate", function () {
  return function (x) {
    var dateToReturn = "";
    var d = x.replace(/-/g, '/');
    var date1 = new Date(d);
    //console.log(date1);
    var date2 = new Date();
    //console.log(date2);
    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
    var diffMin = Math.ceil(timeDiff / (1000 * 60)); // minuty
    //console.log("Minuty "+diffMin);
    if (diffMin > 60) {
      var diffHour = Math.ceil(diffMin / 60);
      dateToReturn = diffHour + " h";
      if (diffHour > 24) {
        var diffDay = Math.ceil(diffHour / 24);
        //console.log("Dni "+diffDay);
        dateToReturn = diffDay + " d";
        if (diffDay > 30) {
          var diffMont = Math.ceil(diffDay / 30);
          dateToReturn = diffMont + " m";
          //console.log("Month " + diffMont);
          if (diffMont > 12) {
            var diffYears = Math.ceil(diffMont / 12);
            //console.log("years " + diffYears);
            return dateToReturn = diffYears + " y";
          }
          else return dateToReturn;
        }
        else return dateToReturn;
      }
      else return dateToReturn;
    }
    else {
      dateToReturn = diffMin + " min"
      return dateToReturn;
    }
  };
});

app.filter("matchStart", function ($rootScope) {
  return function (x) {
    var dateToReturn = "";
    var d = x.replace(/-/g, '/');
    var date1 = new Date(d);
    //console.log(date1);
    var date2 = new Date();
    //console.log(date2);
    return date1 < date2;
  };
});

app.filter("dateFormat", function ($rootScope) {
  return function (x) {
    var dateToReturn = "";
    var d = x.replace(/-/g, '/');
    var date1 = new Date(d);
    // console.log(date1);
    return (date1.getHours() < 10 ? '0' + date1.getHours() : date1.getHours()) + ':' + (date1.getMinutes() < 10 ? '0' + date1.getMinutes() : date1.getMinutes());
  };
});

app.filter("matchTime", function ($rootScope) {
  return function (x) {
    if ($rootScope.liveScore.toggle == 'live') {
      var dateToReturn = "";
      var d = x.replace(/-/g, '/');
      var date1 = new Date(d);
      //console.log(date1);
      var date2 = new Date();
      //console.log(date2);
      var timeDiff = Math.abs(date2.getTime() - date1.getTime());
      var diffMin = Math.ceil(timeDiff / (1000 * 60)); // minuty
      //console.log("Minuty " + diffMin);

      if (diffMin > 45 && diffMin < 60) {
        return dateToReturn = "HT";
      }

      if (diffMin > 60) {
        diffMin = diffMin - 15;
      }

      if (diffMin > 90) {
        return dateToReturn = "FT";
      }

      dateToReturn = diffMin + "'";

      return dateToReturn;
    }
    else {
      //console.log("time:", x);

      var dateToReturn = "";
      var d = x.replace(/-/g, '/');
      var date1 = new Date(d);
      //console.log(date1);
      var date2 = new Date();
      //console.log(date2);

      return (date1.getHours() < 10 ? '0' + date1.getHours() : date1.getHours()) + ':' + (date1.getMinutes() < 10 ? '0' + date1.getMinutes() : date1.getMinutes());
      // if (date1 < date2) {
      //     return "FT";
      // }
      // else {
      //     //console.log(date1.getHours()+":"+date1.getMinutes());
      //     var timeDiff = Math.abs(date1.getTime() - date2.getTime());
      //     var diffMin = Math.ceil(timeDiff / (1000 * 60)); // minuty
      //     //console.log(diffMin);

      //     switch (true) {
      //         case (diffMin > 1440):
      //             dateToReturn = Math.ceil(diffMin / 1440) + "d";
      //             break;

      //         case (diffMin > 60):
      //             dateToReturn = Math.ceil(diffMin / 60) + "h";
      //             break;

      //         case (diffMin < 60):
      //             dateToReturn = diffMin + "'";
      //             break;

      //         default:
      //             break;
      //     }

      // }

      // console.log(date1);
    }
  };
});
