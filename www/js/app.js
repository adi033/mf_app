var app = angular.module('App', ['ionic', 'ionic.native', 'ngCordova'])

app.run(function ($ionicPlatform, $rootScope, $cordovaNetwork, $cordovaNativeStorage, Constants, DataServices, $ionicLoading, $cordovaStatusbar, $cordovaDevice) {
  $ionicPlatform.ready(function () {
    $cordovaNativeStorage.getItem("k").then(function (userId) {
      $rootScope.settings.userId = userId;
      console.log($rootScope.settings.userId);
      Constants.userId = userId;
    }, function (error) {
      console.log(error);
    });

    if (window.cordova) {
      console.log('device');
      // console.log(window.device.uuid);
      //console.log($cordovaDevice.getUUID());

      $timeout(function () {
        navigator.splashscreen.hide()
      }, 500);

      Constants.deviceId = $cordovaDevice.getDevice().uuid;
      console.log("Constants.deviceId ", Constants.deviceId);

      var internetConnected = true;
      $rootScope.$on('$cordovaNetwork:online', function (event, networkState) {
        if (internetConnected) return;

        internetConnected = true;

        if (networkState != "none") {
          $rootScope.onlineState = true;

          for (var key in $rootScope.pageRefresh) {
            $rootScope.pageRefresh[key] = true;
            console.log("key ", key, $rootScope.pageRefresh[key]);
          }

        }
      })

      // listen for Offline event
      $rootScope.$on('$cordovaNetwork:offline', function (event, networkState) {
        if (!internetConnected) return;

        internetConnected = false;
        if (networkState == "none") {
          $rootScope.onlineState = false;

          for (var key in $rootScope.pageRefresh) {
            $rootScope.pageRefresh[key] = false;
            console.log("key ", key, $rootScope.pageRefresh[key]);
          }
        }

        console.log("$rootScope.onlineState ", $rootScope.onlineState);
        //alert("No connection, App can behave stragne");
        //console.log("No connection, App can behave stragne");
      })

      setTimeout(function () {
        // StatusBar.show();
        // StatusBar.backgroundColorByHexString("#ffffff");
        $cordovaStatusbar.overlaysWebView(false);
        //$cordovaStatusbar.style(2);
        //$cordovaStatusbar.styleHex('#FFFFFF') //red
        $cordovaStatusbar.styleColor('black');
        //console.log(StatusBar);
        //StatusBar.styleLightContent();
      }, 3000);

      var notificationOpenedCallback = function (jsonData) {
        console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
      };

      window.plugins.OneSignal
        .startInit("23afadb6-21ea-49c7-a722-12b2ca307d33")
        .handleNotificationOpened(notificationOpenedCallback)
        .endInit();

      window.plugins.OneSignal.getIds(function (ids) {
        console.log("notifications", ids);
        Constants.notificationId = ids.userId;
        $rootScope.login()
      });
    } else {
      console.log('browser');
      Constants.deviceId = '1'
      $rootScope.login()
    }
  });
})

app.run(function ($rootScope, DataServices, $state, Constants, $ionicPlatform, $cordovaNativeStorage, $ionicPopup, $ionicSideMenuDelegate, $cordovaNetwork, $ionicLoading) {
  $rootScope.isAndroid = ionic.Platform.isAndroid();
  $rootScope.isIOS = ionic.Platform.isIOS();

  $rootScope.USERID = undefined;
  $rootScope.$watch(function () {
    return Constants.userId;
  }, function watchCallback(newValue, oldValue) {
    $rootScope.USERID = Constants.userId;
    console.log(" $rootScope.USERID ", $rootScope.USERID);
  }, true);

  $rootScope.onlineState = true;
  $rootScope.pageRefresh = {
    tips: false,
    freebets: false,
    livescore: false,
    settings: false
  }

  $rootScope.liveScore = {};
  $rootScope.liveScore.toggle = undefined;

  $rootScope.loginCheck = function () {
    if ($rootScope.USERID == undefined) {
      return false;
    }
    return true;
  }

  $rootScope.login = () => {
    $ionicLoading.show()
    DataServices.login()
      .success(function (data) {
        console.log("data ", data);
        if (data.result)
          Constants.userId = data.userid;

        console.log("Constants.userId ", Constants.userId);
      })
      .error(function () {
        alert("Something went wrong. Please refresh and try again.");
      })
      .then(function () {
        $ionicLoading.hide();
      })
  }

})


app.config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
  $ionicConfigProvider.tabs.position('bottom');
  $stateProvider
    .state('tabs', {
      url: '/tabs',
      abstract: true,
      templateUrl: 'templates/tabs.html'
    })

    .state('tabs.tips', {
      url: '/tips',
      views: {
        'tabs-tips': {
          templateUrl: 'templates/tips.html',
          controller: 'tips'
        }
      }
    })

    .state('tabs.activeTip', {
      url: "/activeTip",
      params: { post: null },
      views: {
        'tabs-tips': {
          templateUrl: "templates/activeTip.html",
          controller: 'activeTip'
        }
      }
    })

    .state('tabs.liveScores', {
      url: '/liveScores',
      views: {
        'tabs-liveScores': {
          templateUrl: 'templates/liveScores.html',
          controller: 'liveScores'
        }
      }
    })
    .state('tabs.bookieBonuses', {
      url: '/bookieBonuses',
      views: {
        'tabs-bookieBonuses': {
          templateUrl: 'templates/bookieBonuses.html',
          controller: 'bookieBonuses'
        }
      }
    })

    .state('tabs.settings', {
      url: '/settings',
      views: {
        'tabs-settings': {
          templateUrl: 'templates/settings.html',
          controller: 'settings'
        }
      }
    })

    .state('tabs.landingPage', {
      url: '/landingPage',
      views: {
        'tabs-landingPage': {
          templateUrl: 'templates/landingPage.html',
          controller: 'landingPage'
        }
      }
    })

  $urlRouterProvider.otherwise('/tabs/landingPage');

});
